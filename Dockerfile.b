FROM python:3.7.3-alpine3.9

COPY app_b.py /
COPY requirements.txt /
COPY schema.sql /
RUN pip install -r /requirements.txt
RUN apk add sqlite --update
RUN sqlite3 database.db < /schema.sql

RUN addgroup -S pyuser && adduser -S -g pyuser pyuser
USER pyuser

CMD ["python", "-u", "app_b.py"]